# Linux CLI tips

A script that prints bubbles with Linux command-line interface tips. It's best
used as a terminal startup program.

![screenshot](screenshot.png)

## License

MIT
